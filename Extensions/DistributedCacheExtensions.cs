using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Distributed;

namespace RedisDemoApp.Extensions;
public static class DistributedCacheExtensions
{
    public static async Task setRecordAsync<T>(this IDistributedCache cache,
    string recordId,
    T data,
    TimeSpan? absoluteExpiredTime = null,
    TimeSpan? unusedExpiredTime = null
    )
    {
        var options = new DistributedCacheEntryOptions();
        options.AbsoluteExpirationRelativeToNow = absoluteExpiredTime ?? TimeSpan.FromSeconds(60);
        options.SlidingExpiration = unusedExpiredTime;

        var jsonData = JsonSerializer.Serialize(data);
        await cache.SetStringAsync(recordId, jsonData, options);
    }

    public static async Task<T> getRecordAsync<T>(this IDistributedCache cache, string recordId)
    {
        var jsonData = await cache.GetStringAsync(recordId);

        if(jsonData is null)
        {
            return default(T)!;
        }
        return JsonSerializer.Deserialize<T>(jsonData)!;
    }
}