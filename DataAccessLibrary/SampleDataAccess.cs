using Microsoft.Extensions.Caching.Memory;

namespace DataAccessLibrary;

public class SampleDataAccess
{
    private readonly IMemoryCache _memoryCache;
    public SampleDataAccess(IMemoryCache memoryCache)
    {
        _memoryCache = memoryCache;
    }
    public List<EmployeeModel> GetEmployees()
    {
        List<EmployeeModel> employees = new List<EmployeeModel>();
        // {
        //     new (){FirstName="J",LastName="W"},
        //     new (){FirstName="G",LastName="I"},
        //     new (){FirstName="C",LastName="Y"}
        // };
        employees.Add(new EmployeeModel { FirstName = "J", LastName = "W" });
        employees.Add(new EmployeeModel() { FirstName = "G", LastName = "I" });
        employees.Add(new EmployeeModel() { FirstName = "C", LastName = "Y" });
        Thread.Sleep(3000);
        return employees;
    }
    public async Task<List<EmployeeModel>> GetEmployeesAsync()
    {
        List<EmployeeModel> employees = new List<EmployeeModel>();
        employees.Add(new EmployeeModel { FirstName = "J", LastName = "W" });
        employees.Add(new EmployeeModel() { FirstName = "G", LastName = "I" });
        employees.Add(new EmployeeModel() { FirstName = "C", LastName = "Y" });
        await Task.Delay(3000);
        return employees;
    }

    public async Task<List<EmployeeModel>> GetEmployeesCache()
    {
        List<EmployeeModel> employees;

        employees = _memoryCache.Get<List<EmployeeModel>>("employees");

        if (employees is null)
        {
            employees = new List<EmployeeModel>();
            employees.Add(new EmployeeModel { FirstName = "J", LastName = "W" });
            employees.Add(new EmployeeModel() { FirstName = "G", LastName = "I" });
            employees.Add(new EmployeeModel() { FirstName = "C", LastName = "Y" });

            await Task.Delay(3000);
            // _memoryCache.Set<List<EmployeeModel>>("employees", employees);
            _memoryCache.Set("employees",employees,TimeSpan.FromSeconds(10));
        }

        return employees;
    }
}